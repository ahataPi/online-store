package com.example.repository.customer;

import com.example.model.customer.Customer;
import com.example.repository.SoftDeleteEntityRepository;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.PostgresUUIDType;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CustomerRepository extends SoftDeleteEntityRepository<Customer, UUID> {
    private static final String FIND_CUSTOMERS_WITH_BIGGEST_ORDER_HISTORY_QUERY =
            "SELECT customer_id FROM orders group by customer_id\n" +
            "  HAVING COUNT(id) = (SELECT MAX(mycount) " +
            "FROM (SELECT COUNT(id) mycount FROM orders GROUP BY customer_id) as om)";

    public CustomerRepository(Class<Customer> classType, EntityManager entityManager) {
        super(classType, entityManager);
    }

    public List<Customer> findCustomersWithBiggestOrderHistory() {
        Query biggestHistoryCustomers = getEntityManager()
                .createNativeQuery(FIND_CUSTOMERS_WITH_BIGGEST_ORDER_HISTORY_QUERY)
                .unwrap( NativeQuery.class )
                .addScalar( "customer_id", PostgresUUIDType.INSTANCE);
          List<UUID> customerIds = (List<UUID>) biggestHistoryCustomers.getResultList();
        return findByIds(customerIds);
    }

    public List<Customer> findByIds(List<UUID> uuids){
       List<Customer> customers = new ArrayList<>();
       if(!uuids.isEmpty()) {
         for (UUID id : uuids) {
           findById(id).ifPresent(customers::add);
         }
       }
        return customers;
    }
}
