package com.example.repository.order;

import com.example.model.customer.Customer;
import com.example.model.order.Order;
import com.example.repository.SoftDeleteEntityRepository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

public class OrderRepository extends SoftDeleteEntityRepository<Order, UUID> {
    public OrderRepository(Class<Order> classType, EntityManager entityManager) {
        super(classType, entityManager);
    }

    public List<Order> getAllOrdersFor2weeks() {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Order> cq = builder.createQuery(getClassType());
        Root<Order> root = cq.from(getClassType());
        cq.select(root);

        Predicate greaterThan2weeksAgoPredicate = builder.greaterThan(root.get("orderDate"),
                Instant.now().minus(14L, ChronoUnit.DAYS));
        Predicate deletedAtNullablePredicate = builder.isNull(root.get(DELETED_AT_FIELD_NAME));
        Predicate finalPredicate
                = builder.and(greaterThan2weeksAgoPredicate, deletedAtNullablePredicate);

        cq.where(finalPredicate);
        return getEntityManager().createQuery(cq).getResultList();
    }

    public BigDecimal getAveragePriceForLastMonth() {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Double> cq = builder.createQuery(Double.class);
        Root<Order> root = cq.from(Order.class);
        cq.select(builder.avg(root.get("totalPrice")));

        Predicate greaterThan1MonthAgoPredicate = builder.greaterThan(root.get("orderDate"),
                Instant.now().minus(30L, ChronoUnit.DAYS));
        Predicate deletedAtNullablePredicate = builder.isNull(root.get(DELETED_AT_FIELD_NAME));
        Predicate finalPredicate
                = builder.and(greaterThan1MonthAgoPredicate, deletedAtNullablePredicate);
        cq.where(finalPredicate);
        return BigDecimal.valueOf(getEntityManager().createQuery(cq).getSingleResult());
    }

    public List<Order> getAllOrdersForCustomer(Customer customer) {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Order> cq = builder.createQuery(getClassType());
        Root<Order> root = cq.from(getClassType());
        cq.select(root);

        Predicate customerPredicate = builder.equal(root.get("customer"), customer);
        Predicate deletedAtNullablePredicate = builder.isNull(root.get(DELETED_AT_FIELD_NAME));
        Predicate finalPredicate
                = builder.and(customerPredicate, deletedAtNullablePredicate);

        cq.where(finalPredicate);
        return getEntityManager().createQuery(cq).getResultList();
    }

}
