package com.example.repository;

import java.util.List;
import java.util.Optional;

public interface GenericRepository<T, ID> {
    Optional<T> findById(ID id);

    List<T> getAll();

    void insert(T object);

    T update(T object);

    void delete(T object);

    void deleteById(ID id);

    boolean exists(ID id);
}