package com.example.repository;

import com.example.model.SoftDeleteEntity;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

public class SoftDeleteEntityRepository<T extends SoftDeleteEntity, ID> extends AbstractRepository<T, ID> {
    protected static final String DELETED_AT_FIELD_NAME = "deletedAt";

    public SoftDeleteEntityRepository(Class<T> classType, EntityManager entityManager) {
        super(classType, entityManager);
    }

    @Override
    public List<T> getAll() {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> cq = builder.createQuery(getClassType());
        Root<T> root = cq.from(getClassType());
        cq.select(root)
                .where(builder.isNull(root.get(DELETED_AT_FIELD_NAME)));
        return getEntityManager().createQuery(cq).getResultList();
    }

    @Override
    public Optional<T> findById(ID id) {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> cq = builder.createQuery(getClassType());
        Root<T> root = cq.from(getClassType());
        cq.select(root);
        Predicate idEqualityPredicate = builder.equal(root.get("id"), id);
        Predicate deletedAtNullablePredicate = builder.isNull(root.get(DELETED_AT_FIELD_NAME));
        Predicate finalPredicate
                = builder.and(idEqualityPredicate, deletedAtNullablePredicate);
        cq.where(finalPredicate);
        return Optional.ofNullable(getEntityManager().createQuery(cq).getSingleResult());
    }

    @Override
    public void delete(T entity) {
        entity.setDeletedAt(Instant.now());
        super.update(entity);
    }
}
