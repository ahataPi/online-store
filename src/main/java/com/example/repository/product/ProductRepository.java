package com.example.repository.product;

import com.example.model.product.Product;
import com.example.repository.SoftDeleteEntityRepository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class ProductRepository extends SoftDeleteEntityRepository<Product, UUID> {
    private static final String GET_NUMBER_OF_ORDERS_FOR_PRODUCT_QUERY =
            "SELECT COUNT(order_id) FROM order_details GROUP BY product_id HAVING product_id = ?";
    private static final String GET_TOP_10_PRODUCTS =
            "SELECT * FROM products WHERE id IN (SELECT product_id FROM order_details " +
                    "GROUP BY product_id ORDER BY COUNT(order_id) DESC LIMIT 10)";

    public ProductRepository(Class<Product> classType, EntityManager entityManager) {
        super(classType, entityManager);
    }

    public Optional<BigInteger> getNumberOfOrdersForProduct(UUID productId) {
        Query ordersNumberQuery = getEntityManager()
                .createNativeQuery(GET_NUMBER_OF_ORDERS_FOR_PRODUCT_QUERY)
                .setParameter(1, productId.toString());
        BigInteger ordersNumber = (BigInteger) ordersNumberQuery.getSingleResult();
        return Optional.ofNullable(ordersNumber);
    }

    public List<Product> getTop10Products() {
        Query ordersNumberQuery = getEntityManager()
                .createNativeQuery(GET_TOP_10_PRODUCTS, Product.class);
        return (List<Product>) ordersNumberQuery.getResultList();
    }
}
