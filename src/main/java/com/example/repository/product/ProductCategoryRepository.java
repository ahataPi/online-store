package com.example.repository.product;

import com.example.model.product.ProductCategory;
import com.example.repository.SoftDeleteEntityRepository;

import javax.persistence.EntityManager;
import java.util.UUID;

public class ProductCategoryRepository extends SoftDeleteEntityRepository<ProductCategory, UUID> {
    public ProductCategoryRepository(Class<ProductCategory> classType, EntityManager entityManager) {
        super(classType, entityManager);
    }
}
