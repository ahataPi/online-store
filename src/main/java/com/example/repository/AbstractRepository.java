package com.example.repository;

import com.example.util.transaction.TransactionManagerUtil;
import com.example.util.transaction.function.JPATransactionFunction;
import com.example.util.transaction.function.JPATransactionVoidFunction;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<T, ID> implements GenericRepository<T, ID> {

    private final Class<T> classType;
    private final EntityManager entityManager;
    private final TransactionManagerUtil transactionManagerUtil;

    public AbstractRepository(Class<T> classType, EntityManager entityManager) {
        this.classType = classType;
        this.entityManager = entityManager;
        this.transactionManagerUtil = new TransactionManagerUtil(entityManager);
    }

    public Class<T> getClassType() {
        return classType;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public List<T> getAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> cq = builder.createQuery(classType);
        Root<T> root = cq.from(classType);
        cq.select(root);
        return entityManager.createQuery(cq).getResultList();
    }

    @Override
    public Optional<T> findById(ID id) {
        return Optional.ofNullable(entityManager.find(classType, id));
    }

    @Override
    public void insert(T entity) {
        transactionManagerUtil.doInJPA((JPATransactionVoidFunction) entityManager -> entityManager.persist(entity));
    }

    @Override
    public T update(T entity) {
        return transactionManagerUtil.doInJPA((JPATransactionFunction<T>) entityManager -> entityManager.merge(entity));
    }

    @Override
    public void delete(T entity) {
        transactionManagerUtil.doInJPA((JPATransactionVoidFunction) entityManager -> delete(entityManager, entity));
    }

    private void delete(EntityManager entityManager, T entity) {
        entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
    }

    @Override
    public void deleteById(ID id) {
        transactionManagerUtil.doInJPA((JPATransactionVoidFunction) entityManager -> deleteById(entityManager, id));
    }

    private void deleteById(EntityManager entityManager, ID id) {
        delete(entityManager, findById(id).orElseThrow(() -> new EntityNotFoundException(
                String.format("There is no entity of type %s with id %s in DB!", classType, id))));
    }

    @Override
    public boolean exists(ID id) {
        return findById(id).isPresent();
    }
}
