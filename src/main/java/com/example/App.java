package com.example;

import com.example.model.customer.Customer;
import com.example.model.order.Order;
import com.example.model.product.Product;
import com.example.model.product.ProductCategory;
import com.example.repository.AbstractRepository;
import com.example.repository.customer.CustomerRepository;
import com.example.repository.order.OrderRepository;
import com.example.repository.product.ProductCategoryRepository;
import com.example.repository.product.ProductRepository;
import com.example.service.customer.CustomerService;
import com.example.service.dto.customer.CustomerDTO;
import com.example.service.dto.order.OrderDTO;
import com.example.service.dto.order.OrderDetailsDTO;
import com.example.service.dto.product.ProductCategoryDTO;
import com.example.service.dto.product.ProductDTO;
import com.example.service.order.OrderService;
import com.example.service.product.ProductCategoryService;
import com.example.service.product.ProductService;
import com.github.javafaker.Faker;
import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRepository.class);

    public static void main(String[] args) {
        Flyway flyway = Flyway.configure()
                .dataSource("jdbc:postgresql://pgdb:5432/onlinestore", "ahata", "12345")
                .locations("classpath:db/migration")
                .load();
        flyway.migrate();

        EntityManagerFactory entityManagerFactory
                = Persistence.createEntityManagerFactory("online-store");
        EntityManager em = entityManagerFactory.createEntityManager();

        productCategoryCRUDTesting(em);
        productCRUDTesting(em);
        customerCRUDTesting(em);
        orderCRUDTesting(em);

        ProductRepository productRepository = new ProductRepository(Product.class, em);
        ProductService productService = new ProductService(productRepository);

        UUID productId = productService.getAll().get(0).getId();
        LOGGER.info("Getting number of orders for product with id {}: \n {}", productId, productService.getNumberOfOrdersForProduct(productId));
        LOGGER.info("Getting top 10 products: \n {}", productService.getTop10Products().toString());
    }

    public static void productCategoryCRUDTesting(EntityManager em) {
        Faker faker = new Faker();
        ProductCategoryRepository repository = new ProductCategoryRepository(ProductCategory.class, em);
        ProductCategoryService productCategoryService = new ProductCategoryService(repository);

        for (int i = 0; i < 10; i++) {
            ProductCategoryDTO productCategory = new ProductCategoryDTO();
            productCategory.setCategoryName(faker.food().fruit());
            productCategoryService.insert(productCategory);
        }

        LOGGER.info("Getting all entities after insertion: \n {}", productCategoryService.getAll().toString());

        ProductCategoryDTO pr = productCategoryService.get(productCategoryService.getAll().get(0).getId());
        LOGGER.info("Entity that is going to be updated: \n {}", pr);
        pr.setCategoryName(faker.zelda().character());
        LOGGER.info("Entity after being updated: \n {}", productCategoryService.update(pr));
        LOGGER.info("Getting all entities after update: \n {}", productCategoryService.getAll().toString());

        productCategoryService.delete(pr);
        LOGGER.info("Getting all entities after delete: \n {}", productCategoryService.getAll().toString());
    }

    public static void productCRUDTesting(EntityManager em) {
        Faker faker = new Faker();
        ProductRepository productRepository = new ProductRepository(Product.class, em);
        ProductService productService = new ProductService(productRepository);

        for (int i = 0; i < 5; i++) {
            ProductDTO product = new ProductDTO();
            product.setName(faker.commerce().productName());
            product.setPrice(new BigDecimal(faker.commerce().price()));
            product.setDescription(faker.commerce().material());

            ProductCategoryDTO productCategory = new ProductCategoryDTO();
            productCategory.setCategoryName(faker.food().fruit());

            product.setCategory(productCategory);
            productService.insert(product);
        }

        LOGGER.info("Getting all entities after insertion: \n {}", productService.getAll().toString());

        ProductDTO pr = productService.get(productService.getAll().get(0).getId());
        LOGGER.info("Entity that is going to be updated: \n {}", pr);
        pr.setName(faker.zelda().character());
        LOGGER.info("Entity after being updated: \n {}", productService.update(pr));
        LOGGER.info("Getting all entities after update: \n {}", productService.getAll().toString());

        productService.delete(pr);
        LOGGER.info("Getting all entities after delete: \n {}", productService.getAll().toString());
    }

    public static void customerCRUDTesting(EntityManager em) {
        Faker faker = new Faker();
        OrderRepository orderRepository = new OrderRepository(Order.class, em);
        CustomerRepository customerRepository = new CustomerRepository(Customer.class, em);
        CustomerService customerService = new CustomerService(customerRepository, orderRepository);

        for (int i = 0; i < 5; i++) {
            CustomerDTO customer = new CustomerDTO();
            customer.setFirstName(faker.funnyName().name());
            customer.setAddress(faker.address().fullAddress());
            customer.setBirthDate(faker.date().birthday().toInstant());
            customer.setEmail(faker.internet().safeEmailAddress());
            customer.setGender("MALE");

            customerService.insert(customer);
        }

        LOGGER.info("Getting all entities after insertion: \n {}", customerService.getAll().toString());

        CustomerDTO customer = customerService.get(customerService.getAll().get(0).getId());
        LOGGER.info("Entity that is going to be updated: \n {}", customer);
        customer.setLastName(faker.funnyName().name());
        LOGGER.info("Entity after being updated: \n {}", customerService.update(customer));
        LOGGER.info("Getting all entities after update: \n {}", customerService.getAll().toString());

        customerService.delete(customer);
        LOGGER.info("Getting all entities after delete: \n {}", customerService.getAll().toString());
    }

    public static void orderCRUDTesting(EntityManager em) {
        Faker faker = new Faker();
        OrderRepository orderRepository = new OrderRepository(Order.class, em);
        OrderService orderService = new OrderService(orderRepository);

        CustomerRepository customerRepository = new CustomerRepository(Customer.class, em);
        CustomerService customerService = new CustomerService(customerRepository, orderRepository);

        ProductRepository productRepository = new ProductRepository(Product.class, em);
        ProductService productService = new ProductService(productRepository);

        for (int i = 0; i < 10; i++) {
            OrderDTO order = new OrderDTO();
            order.setOrderDate(Instant.now());
            order.setDeliveryAddress(faker.address().fullAddress());
            order.setCustomer(customerService.getAll().get(0));
            order.setStatus("NEW");

            List<OrderDetailsDTO> orderDetailsList = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                OrderDetailsDTO orderDetails = new OrderDetailsDTO();
                orderDetails.setProduct(productService.getAll().get(j));
                orderDetailsList.add(orderDetails);
            }
            order.setOrderDetails(orderDetailsList);

            orderService.insert(order);
        }

        LOGGER.info("Getting all entities after insertion: \n {}", orderService.getAll().toString());

        OrderDTO order = orderService.get(orderService.getAll().get(0).getId());
        LOGGER.info("Entity that is going to be updated: \n {}", order);
        order.setStatus("CANCELLED");
        LOGGER.info("Entity after being updated: \n {}", orderService.update(order));
        LOGGER.info("Getting all entities after update: \n {}", orderService.getAll().toString());

        orderService.delete(order);
        LOGGER.info("Getting all entities after delete: \n {}", orderService.getAll().toString());

        LOGGER.info("Getting all orders for 2 weeks: \n {}", orderService.getAllOrdersFor2weeks().toString());

        LOGGER.info("Getting average price of orders for last month: \n {}", orderService.getAveragePriceForLastMonth().toString());

        LOGGER.info("Getting customers with biggest order history: \n {}", customerService.getCustomerWithBiggestOrderHistory().toString());
    }
}
