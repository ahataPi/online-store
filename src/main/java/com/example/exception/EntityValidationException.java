package com.example.exception;

public class EntityValidationException extends Exception {

    public EntityValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityValidationException(String message) {
        super(message);
    }
}