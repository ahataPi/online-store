package com.example.service;

import com.example.repository.AbstractRepository;
import com.example.service.dto.mapper.AbstractMapper;
import com.example.util.validation.ValidationUtil;
import jakarta.validation.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public abstract class AbstractService<T, ID, DTO_T> implements GenericService<T, ID, DTO_T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractService.class);

    private final AbstractRepository<T, ID> repository;
    private final AbstractMapper<T, DTO_T> mapper;
    private final ValidationUtil<DTO_T> validationUtil;

    public AbstractService(AbstractRepository<T, ID> repository, Supplier<AbstractMapper<T, DTO_T>> getMapperInstance) {
        this.repository = repository;
        this.mapper = getMapperInstance.get();
        this.validationUtil = new ValidationUtil<>();
    }

    public AbstractRepository<T, ID> getRepository() {
        return repository;
    }

    public AbstractMapper<T, DTO_T> getMapper() {
        return mapper;
    }

    @Override
    public DTO_T get(ID id) {
        T entity = repository.findById(id).orElseThrow(() -> new EntityNotFoundException(
                String.format("There is no entity of type %s with id %s in DB!", repository.getClassType(), id)));
        return mapper.entityToEntityDTO(entity);
    }

    @Override
    public List<DTO_T> getAll() {
        LOGGER.info(String.format("Getting of all entities of type %s: \n", repository.getClassType()));
        List<DTO_T> entitiesDTO = new ArrayList<>();
        for (T entity : repository.getAll()) {
            entitiesDTO.add(mapper.entityToEntityDTO(entity));
        }
        return entitiesDTO;
    }

    @Override
    public void insert(DTO_T entity) {
        LOGGER.info(String.format("Inserting of entity of type %s: entity is %s \n", repository.getClassType(), entity.toString()));
        try {
            validationUtil.validate(entity);
            repository.insert(mapper.entityDTOToEntity(entity));
        } catch (ValidationException e) {
            LOGGER.error("The error list while entity validation for insert is:\n {}", e.getMessage());
        }
    }

    @Override
    public DTO_T update(DTO_T entity) {
        LOGGER.info(String.format("Updating of entity of type %s: new entity is %s \n", repository.getClassType(), entity.toString()));
        DTO_T result = entity;
        try {
            validationUtil.validate(entity);
            T updatedEntity = mapper.entityDTOToEntity(entity);
            result = mapper.entityToEntityDTO(repository.update(updatedEntity));
        } catch (ValidationException e) {
            LOGGER.error("The error list while entity validation for update is:\n {}", e.getMessage());
        }
        return result;
    }

    @Override
    public void delete(DTO_T entity) {
        LOGGER.info(String.format("Deleting of entity of type %s: entity is %s \n", repository.getClassType(), entity.toString()));
        repository.delete(mapper.entityDTOToEntity(entity));
    }

    @Override
    public void deleteById(ID id) {
        LOGGER.info(String.format("Deleting of entity of type %s: entity id is %s \n", repository.getClassType(), id));
        repository.deleteById(id);
    }

    @Override
    public boolean exists(ID id) {
        LOGGER.info(String.format("Checking of existence of entity of type %s: entity id is %s \n", repository.getClassType(), id));
        return repository.exists(id);
    }
}
