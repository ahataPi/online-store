package com.example.service;

import java.util.List;

public interface GenericService<T, ID, DTO_T> {
    DTO_T get(ID id);

    List<DTO_T> getAll();

    void insert(DTO_T entity);

    DTO_T update(DTO_T entity);

    void delete(DTO_T entity);

    void deleteById(ID id);

    boolean exists(ID id);
}