package com.example.service.product;

import com.example.model.product.ProductCategory;
import com.example.repository.AbstractRepository;
import com.example.service.AbstractService;
import com.example.service.dto.mapper.product.ProductCategoryMapper;
import com.example.service.dto.product.ProductCategoryDTO;

import java.util.UUID;

public class ProductCategoryService extends AbstractService<ProductCategory, UUID, ProductCategoryDTO> {
    public ProductCategoryService(AbstractRepository<ProductCategory, UUID> repository) {
        super(repository, ProductCategoryMapper::getInstance);
    }
}
