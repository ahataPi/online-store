package com.example.service.product;

import com.example.model.product.Product;
import com.example.repository.AbstractRepository;
import com.example.repository.product.ProductRepository;
import com.example.service.AbstractService;
import com.example.service.dto.mapper.product.ProductMapper;
import com.example.service.dto.product.ProductDTO;

import javax.persistence.EntityNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProductService extends AbstractService<Product, UUID, ProductDTO> {
    public ProductService(AbstractRepository<Product, UUID> repository) {
        super(repository, ProductMapper::getInstance);
    }

    @Override
    public ProductRepository getRepository() {
        return (ProductRepository) super.getRepository();
    }

    public BigInteger getNumberOfOrdersForProduct(UUID productId) {
        ProductDTO product = get(productId);
        return getRepository().getNumberOfOrdersForProduct(product.getId()).orElseThrow(() -> new EntityNotFoundException(
                String.format("There is no orders with Product (id = %s) in DB!", product.getId())));
    }

    public List<ProductDTO> getTop10Products() {
        List<ProductDTO> productsDTO = new ArrayList<>();
        for (Product entity : getRepository().getTop10Products()){
            productsDTO.add(getMapper().entityToEntityDTO(entity));
        }
        return productsDTO;
    }
}
