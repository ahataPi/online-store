package com.example.service.dto.mapper.customer;

import com.example.model.customer.Customer;
import com.example.service.dto.customer.CustomerDTO;
import com.example.service.dto.mapper.AbstractMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CustomerMapper extends AbstractMapper<Customer, CustomerDTO> {
    static CustomerMapper getInstance() {
        return Mappers.getMapper(CustomerMapper.class);
    }
}
