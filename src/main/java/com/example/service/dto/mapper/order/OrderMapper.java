package com.example.service.dto.mapper.order;

import com.example.model.order.Order;
import com.example.model.order.OrderDetails;
import com.example.service.dto.mapper.AbstractMapper;
import com.example.service.dto.mapper.customer.CustomerMapper;
import com.example.service.dto.order.OrderDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {CustomerMapper.class, OrderDetailsMapper.class})
public interface OrderMapper extends AbstractMapper<Order, OrderDTO> {
    static OrderMapper getInstance() {
        return Mappers.getMapper(OrderMapper.class);
    }

    @AfterMapping
    static void afterMapping(@MappingTarget Order target) {
        for (OrderDetails orderDetails : target.getOrderDetails()) {
            orderDetails.setOrder(target);
        }
    }
}
