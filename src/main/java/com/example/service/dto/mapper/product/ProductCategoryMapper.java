package com.example.service.dto.mapper.product;

import com.example.model.product.ProductCategory;
import com.example.service.dto.mapper.AbstractMapper;
import com.example.service.dto.product.ProductCategoryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductCategoryMapper extends AbstractMapper<ProductCategory, ProductCategoryDTO> {
    static ProductCategoryMapper getInstance() {
        return Mappers.getMapper(ProductCategoryMapper.class);
    }
}
