package com.example.service.dto.mapper.customer;

import com.example.model.customer.Customer;
import com.example.model.order.Order;
import com.example.service.dto.customer.CustomerWithOrdersDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CustomerWithOrdersMapper {
  CustomerWithOrdersDTO getCustomerWithOrders(Customer customer, List<Order> orders);

  static CustomerWithOrdersMapper getInstance() {
    return Mappers.getMapper(CustomerWithOrdersMapper.class);
  }
}
