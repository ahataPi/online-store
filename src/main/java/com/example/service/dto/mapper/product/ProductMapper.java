package com.example.service.dto.mapper.product;

import com.example.model.product.Product;
import com.example.service.dto.mapper.AbstractMapper;
import com.example.service.dto.product.ProductDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {ProductCategoryMapper.class})
public interface ProductMapper extends AbstractMapper<Product, ProductDTO> {
    static ProductMapper getInstance() {
        return Mappers.getMapper(ProductMapper.class);
    }
}
