package com.example.service.dto.mapper.order;

import com.example.model.order.OrderDetails;
import com.example.service.dto.mapper.AbstractMapper;
import com.example.service.dto.mapper.product.ProductMapper;
import com.example.service.dto.order.OrderDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {ProductMapper.class})
public interface OrderDetailsMapper extends AbstractMapper<OrderDetails, OrderDetailsDTO> {
    static OrderDetailsMapper getInstance() {
        return Mappers.getMapper(OrderDetailsMapper.class);
    }
}
