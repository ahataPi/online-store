package com.example.service.dto.mapper;

public interface AbstractMapper<T, DTO_T> {
    DTO_T entityToEntityDTO(T entity);
    T entityDTOToEntity(DTO_T entityDTO);
}
