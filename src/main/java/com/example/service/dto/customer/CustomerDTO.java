package com.example.service.dto.customer;

import jakarta.validation.constraints.*;
import lombok.*;

import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CustomerDTO {
    private UUID id;

    @Size(min = 3, max = 20, message = "{customer.name.invalid}")
    @NotEmpty(message = "{customer.name.empty}")
    private String firstName;

    private String lastName;

    @Email(message = "{customer.email.invalid}")
    @NotEmpty(message = "{customer.email.empty}")
    private String email;

    @NotEmpty(message = "{address.empty}")
    @Size(min = 2, max = 300, message = "{address.invalid}")
    private String address;

    private String gender;

    @NotNull(message = "{customer.birthdate.null}")
    @Past(message = "{customer.birthdate.past}")
    private Instant birthDate;
}
