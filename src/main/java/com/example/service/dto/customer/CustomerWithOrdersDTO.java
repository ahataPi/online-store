package com.example.service.dto.customer;

import com.example.service.dto.order.OrderDTO;
import jakarta.validation.Valid;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class CustomerWithOrdersDTO extends CustomerDTO {
  private List<@Valid OrderDTO> orders = new ArrayList<>();
}
