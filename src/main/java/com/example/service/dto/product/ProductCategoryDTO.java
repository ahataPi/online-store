package com.example.service.dto.product;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProductCategoryDTO {
    private UUID id;

    @Size(min = 3, max = 10, message = "{product.category.name.invalid}")
    @NotEmpty(message = "{product.category.name.empty}")
    private String categoryName;
}
