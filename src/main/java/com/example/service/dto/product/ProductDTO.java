package com.example.service.dto.product;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class ProductDTO {
    private UUID id;

    @Size(min = 3, max = 100, message = "{product.name.invalid}")
    @NotEmpty(message = "{product.name.empty}")
    private String name;

    @Positive(message = "{product.price.positive}")
    @DecimalMax(value = "100000", message = "{product.price.max}")
    @Digits(integer=5, fraction=2, message = "{product.price.digits}")
    private BigDecimal price;

    @Size(max = 1000, message = "{product.description.invalid}")
    private String description;

    @Valid
    private ProductCategoryDTO category;
}
