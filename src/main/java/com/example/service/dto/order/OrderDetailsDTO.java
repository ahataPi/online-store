package com.example.service.dto.order;

import com.example.service.dto.product.ProductDTO;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Positive;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class OrderDetailsDTO {
    @Valid
    private ProductDTO product;

    @Positive(message = "{order.details.count.positive}")
    private int count = 1;
}
