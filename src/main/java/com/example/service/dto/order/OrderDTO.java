package com.example.service.dto.order;

import com.example.service.dto.customer.CustomerDTO;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class OrderDTO {
    private UUID id;

    @PastOrPresent(message = "{order.date.past.present}")
    private Instant orderDate;

    @Positive(message = "{order.totalPrice.positive}")
    private BigDecimal totalPrice;

    @NotEmpty(message = "{address.empty}")
    @Size(min = 2, max = 300, message = "{address.invalid}")
    private String deliveryAddress;

    @Valid
    private CustomerDTO customer;

    private String status;

    private List<@Valid OrderDetailsDTO> orderDetails = new ArrayList<>();
}
