package com.example.service.order;

import com.example.model.order.Order;
import com.example.repository.AbstractRepository;
import com.example.repository.order.OrderRepository;
import com.example.service.AbstractService;
import com.example.service.dto.mapper.order.OrderMapper;
import com.example.service.dto.order.OrderDTO;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class OrderService extends AbstractService<Order, UUID, OrderDTO> {
    public OrderService(AbstractRepository<Order, UUID> repository) {
        super(repository, OrderMapper::getInstance);
    }

    @Override
    public OrderRepository getRepository() {
        return (OrderRepository) super.getRepository();
    }

    public List<Order> getAllOrdersFor2weeks() {
        return getRepository().getAllOrdersFor2weeks();
    }

    public BigDecimal getAveragePriceForLastMonth() {
        return getRepository().getAveragePriceForLastMonth();
    }
}
