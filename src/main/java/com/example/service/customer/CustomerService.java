package com.example.service.customer;

import com.example.model.customer.Customer;
import com.example.model.order.Order;
import com.example.repository.AbstractRepository;
import com.example.repository.customer.CustomerRepository;
import com.example.repository.order.OrderRepository;
import com.example.service.AbstractService;

import com.example.service.dto.customer.CustomerDTO;
import com.example.service.dto.customer.CustomerWithOrdersDTO;
import com.example.service.dto.mapper.customer.CustomerMapper;
import com.example.service.dto.mapper.customer.CustomerWithOrdersMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CustomerService extends AbstractService<Customer, UUID, CustomerDTO> {
    private final OrderRepository orderRepository;
    private final CustomerWithOrdersMapper customerWithOrdersMapper = CustomerWithOrdersMapper.getInstance();

    public CustomerService(AbstractRepository<Customer, UUID> repository, OrderRepository orderRepository) {
        super(repository, CustomerMapper::getInstance);
        this.orderRepository = orderRepository;
    }

    @Override
    public CustomerRepository getRepository() {
        return (CustomerRepository) super.getRepository();
    }

    public List<CustomerWithOrdersDTO> getCustomerWithBiggestOrderHistory() {
       List<Customer> customers = getRepository().findCustomersWithBiggestOrderHistory();
       List<CustomerWithOrdersDTO> result = new ArrayList<>();
       for(Customer customer: customers) {
           List<Order> orders = orderRepository.getAllOrdersForCustomer(customer);
            result.add(customerWithOrdersMapper.getCustomerWithOrders(customer, orders));
       }
       return result;
    }
}
