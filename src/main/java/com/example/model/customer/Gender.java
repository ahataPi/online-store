package com.example.model.customer;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
