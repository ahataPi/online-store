package com.example.model.order;

import com.example.model.SoftDeleteEntity;
import com.example.model.customer.Customer;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "orders")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"}, callSuper = false)
@ToString
public class Order extends SoftDeleteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "number")
    private long orderNumber;
    @Column(name = "order_date")
    private Instant orderDate;
    @Column(name = "total_price")
    private BigDecimal totalPrice;
    @Column(name="address")
    private String deliveryAddress;
    @ManyToOne
    @JoinColumn(name="customer_id")
    private Customer customer;

    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(mappedBy="order", cascade = CascadeType.ALL)
    private List<OrderDetails> orderDetails = new ArrayList<>();

    @PreUpdate
    @PrePersist
    void calculateTotalPrice() {
        totalPrice = BigDecimal.ZERO;
        for (OrderDetails orderDetail : orderDetails) {
            BigDecimal priceOfProductPosition = orderDetail.getProduct().getPrice();
            BigDecimal totalPriceOfPosition = priceOfProductPosition.multiply(new BigDecimal(orderDetail.getCount()));
            totalPrice = totalPrice.add(totalPriceOfPosition);
        }
    }
}
