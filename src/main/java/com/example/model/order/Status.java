package com.example.model.order;

public enum Status {
    NEW,
    PROCESSING,
    ON_HOLD,
    COMPLETED,
    CANCELLED
}
