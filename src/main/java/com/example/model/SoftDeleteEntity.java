package com.example.model;

import lombok.*;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PreUpdate;
import java.time.Instant;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SoftDeleteEntity {
    @Column(name = "created_at", updatable=false)
    @Generated(GenerationTime.INSERT)
    private Instant createdAt;

    @Column(name = "updated_at")
    @Generated(GenerationTime.INSERT)
    private Instant updatedAt;

    @Column(name = "deleted_at")
    public Instant deletedAt;

    @PreUpdate
    void preUpdate(){
        this.updatedAt = Instant.now();
    }
}
