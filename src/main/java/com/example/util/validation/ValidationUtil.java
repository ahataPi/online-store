package com.example.util.validation;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.ValidationException;
import jakarta.validation.Validator;
import org.hibernate.validator.messageinterpolation.ParameterMessageInterpolator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ValidationUtil<T> {
//    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    private final Validator validator = Validation.byDefaultProvider()
            .configure()
            .messageInterpolator(new ParameterMessageInterpolator())
            .buildValidatorFactory()
            .getValidator();

    public void validate(T entity) {
        List<String> errors = new ArrayList<>();
        Set<ConstraintViolation<T>> cvs = validator.validate(entity);
        if (!cvs.isEmpty()) {
            for (ConstraintViolation<T> cv : cvs) {
                String error = cv.getPropertyPath() + " " + cv.getMessage() + "\n";
                errors.add(error);
            }
        }
        if (!errors.isEmpty()) {
            throw new ValidationException(errors.toString());
        }
    }
}

