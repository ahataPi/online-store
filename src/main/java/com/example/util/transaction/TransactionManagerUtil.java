package com.example.util.transaction;

import com.example.util.transaction.function.JPATransactionFunction;
import com.example.util.transaction.function.JPATransactionVoidFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class TransactionManagerUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionManagerUtil.class);

    private final EntityManager entityManager;

    public TransactionManagerUtil(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public <T> T doInJPA(JPATransactionFunction<T> function) {
        T result;
        EntityTransaction txn = null;
        try {
            function.beforeTransactionCompletion();
            txn = entityManager.getTransaction();
            txn.begin();
            result = function.apply(entityManager);
            if (!txn.getRollbackOnly()) {
                txn.commit();
            } else {
                try {
                    txn.rollback();
                } catch (Exception e) {
                    LOGGER.error("Rollback failure", e);
                }
            }
        } catch (Throwable t) {
            if (txn != null && txn.isActive()) {
                try {
                    txn.rollback();
                } catch (Exception e) {
                    LOGGER.error("Rollback failure", e);
                }
            }
            throw t;
        } finally {
            function.afterTransactionCompletion();
        }
        return result;
    }

    public void doInJPA(JPATransactionVoidFunction function) {
        EntityTransaction txn = null;
        try {
            function.beforeTransactionCompletion();
            txn = entityManager.getTransaction();
            txn.begin();
            function.accept(entityManager);
            if (!txn.getRollbackOnly()) {
                txn.commit();
            } else {
                try {
                    txn.rollback();
                } catch (Exception e) {
                    LOGGER.error("Rollback failure", e);
                }
            }
        } catch (Throwable t) {
            if (txn != null && txn.isActive()) {
                try {
                    txn.rollback();
                } catch (Exception e) {
                    LOGGER.error("Rollback failure", e);
                }
            }
            throw t;
        } finally {
            function.afterTransactionCompletion();
        }
    }
}
