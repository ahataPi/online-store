CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE product_categories (
                                    id uuid DEFAULT uuid_generate_v4(),
                                    name VARCHAR NOT NULL,
                                    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
                                    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
                                    deleted_at TIMESTAMP WITH TIME ZONE,

                                    CONSTRAINT pk_product_categories PRIMARY KEY (id)
);

CREATE TABLE products (
                          id uuid DEFAULT uuid_generate_v4(),
                          name VARCHAR NOT NULL,
                          price NUMERIC(5,2) NOT NULL,
                          description text,
                          category_id uuid NOT NULL,
                          created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
                          updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
                          deleted_at TIMESTAMP WITH TIME ZONE,

                          CONSTRAINT pk_products PRIMARY KEY (id),
                          CONSTRAINT fk_products_prod_categories FOREIGN KEY (category_id) REFERENCES  Product_Categories (id)
);
CREATE INDEX fk_products_categories_index ON products (category_id);

create type GENDER as enum('MALE', 'FEMALE', 'OTHER');

CREATE TABLE customers (
                           id uuid DEFAULT uuid_generate_v4(),
                           first_name VARCHAR NOT NULL,
                           last_name VARCHAR,
                           email VARCHAR NOT NULL,
                           address VARCHAR NOT NULL,
                           gender GENDER,
                           birth_date DATE,
                           created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
                           updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
                           deleted_at TIMESTAMP WITH TIME ZONE,

                           CONSTRAINT pk_customer PRIMARY KEY (id)
);

create type STATUS as enum('NEW', 'PROCESSING', 'ON_HOLD', 'COMPLETED', 'CANCELLED');

CREATE TABLE orders (
                        id uuid DEFAULT uuid_generate_v4(),
                        number integer NOT NULL,
                        order_date TIMESTAMP WITH TIME ZONE,
                        total_price NUMERIC(5,2),
                        address VARCHAR,
                        customer_id uuid NOT NULL,
                        status STATUS NOT NULL,
                        created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
                        updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
                        deleted_at TIMESTAMP WITH TIME ZONE,

                        CONSTRAINT pk_orders PRIMARY KEY (id),
                        CONSTRAINT fk_orders_customers FOREIGN KEY (customer_id) REFERENCES  customers (id)
);
CREATE INDEX fk_orders_cutomers_index ON orders (customer_id);


CREATE TABLE order_details (
                               order_id uuid NOT NULL,
                               product_id uuid NOT NULL,
                               count integer NOT NULL,
                               CONSTRAINT fk_order_details_orders FOREIGN KEY (order_id) REFERENCES orders (id) ON DELETE CASCADE,
                               CONSTRAINT fk_order_details_products FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE RESTRICT,
                               CONSTRAINT pk_order_details PRIMARY KEY (order_id, product_id)
);
